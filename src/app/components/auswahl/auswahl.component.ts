import {Component, Output, EventEmitter, Input} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { ChoosetenantComponent } from './choosetenant/choosetenant.component';
import {Tenant} from '../../models/tenant.model';
import {BookingAccount} from '../../models/bookingAccount.model';
import {Property} from '../../models/property.model';
import {Flat} from '../../models/flat.model';
import {Transaction} from '../../models/transactions.model';
import {ChoosePropertyComponent} from './chooseproperty/choose-property.component';
import {CreateEntryComponent} from '../create-entry/create-entry.component';


@Component({
  selector: 'app-auswahl',
  templateUrl: './auswahl.component.html',
  styleUrls: ['./auswahl.component.css']
})
export class AuswahlComponent {
  @Output() sendDataEvent = new EventEmitter<string>();
  @Input() data: Tenant | BookingAccount | Property | Flat | Transaction | undefined;
  dialogRef: MatDialogRef<any> | undefined;
  tenantId: number | undefined;
  tableType: string = 'tenant';

  constructor(private dialog: MatDialog) { }

  onCreate(){
    this.dialogRef =  this.dialog.open(CreateEntryComponent, {data: this.tableType});

    this.dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.sendDataEvent.emit(result);
      }
    });
  }

  openDialog(type: string) {
    if (type === 'accounting') {
      this.dialogRef = this.dialog.open(ChoosetenantComponent);

      this.dialogRef.afterClosed().subscribe(result => {
        this.sendDataEvent.emit(result);
      });
      this.tableType = 'accounting Service';
    } else if (type === 'overview') {
      this.dialogRef = this.dialog.open(ChoosePropertyComponent);

      this.dialogRef.afterClosed().subscribe(result => {
        this.sendDataEvent.emit(result);
      });
    }
  }

  choose(value: string) {
    this.sendDataEvent.emit(value);
    this.tableType = value;
  }
}
