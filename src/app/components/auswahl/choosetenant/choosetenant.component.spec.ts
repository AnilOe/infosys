import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoosetenantComponent } from './choosetenant.component';

describe('ChoosetenantComponent', () => {
  let component: ChoosetenantComponent;
  let fixture: ComponentFixture<ChoosetenantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoosetenantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoosetenantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
