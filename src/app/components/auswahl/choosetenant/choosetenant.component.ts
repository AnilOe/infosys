import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../../service/api.service';
import {Tenant} from '../../../models/tenant.model';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-choosetenant',
  templateUrl: './choosetenant.component.html',
  styleUrls: ['./choosetenant.component.css']
})
export class ChoosetenantComponent implements OnInit {
  selectedGroup: any;
  selectedYear: string | undefined;
  constructor(private apiService: ApiService, private matDialog: MatDialogRef<ChoosetenantComponent>) {
  }
  tenants: Tenant[] = [];

  ngOnInit() {
    this.apiService.getTenants().subscribe(tenants => {
      if (tenants)
      this.tenants = tenants;
    });
  }

  selectTenant() {
    const accounting = {tenant: this.selectedGroup, year: this.selectedYear};
      this.matDialog.close(accounting);
  }
}
