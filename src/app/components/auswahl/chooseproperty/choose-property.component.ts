import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../../service/api.service';
import {MatDialogRef} from '@angular/material/dialog';
import {Property} from '../../../models/property.model';

@Component({
  selector: 'app-choose-property',
  templateUrl: './choose-property.component.html',
  styleUrls: ['./choose-property.component.css']
})
export class ChoosePropertyComponent implements OnInit {
  selectedGroup: any;
  properties: Property[] = [];
  constructor(private apiService: ApiService, private dialogRef: MatDialogRef<ChoosePropertyComponent>) { }

  ngOnInit() {
    this.apiService.getProperties().subscribe(properties => {
      this.properties = properties;
    });
  }

  selectProperty() {
    this.dialogRef.close(this.selectedGroup);
  }
}
