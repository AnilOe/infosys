import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Flat} from '../../models/flat.model';
import {ApiService} from '../../service/api.service';
import {Owner} from '../../models/owner.model';
import {Property} from '../../models/property.model';


@Component({
  selector: 'app-create-entry',
  templateUrl: './create-entry.component.html',
  styleUrls: ['./create-entry.component.css']
})
export class CreateEntryComponent implements OnInit {
  object: FormGroup = new FormGroup({});
  $flats: Flat[] | undefined;
  $properties: Property[];
  $owners: Owner[] | undefined;
  properties: string[];
  foreignKey: any;

  constructor(@Inject(MAT_DIALOG_DATA) public data: string, private apiService: ApiService, private matDialog: MatDialogRef<CreateEntryComponent>) {}

  ngOnInit(): void {
    switch (this.data) {
      case 'tenant':
        this.object = new FormGroup({
          firstName: new FormControl('', [Validators.required]),
          lastName: new FormControl(''),
          phoneNumber: new FormControl(null),
          amountOfPeople: new FormControl(null),
        });
        this.apiService.getFlats().subscribe(flats => {
          this.$flats = flats;
        });
        this.properties = Object.keys(this.object.controls);
        break;
      case 'property':
        this.object = new FormGroup({
          StreetName: new FormControl(''),
          HouseNumber: new FormControl(null),
          PostalCode: new FormControl(null),
          City: new FormControl(''),
          Country: new FormControl(''),
        });
        this.apiService.getOwners().subscribe(owners => {
          this.$owners = owners;
        });
        this.properties = Object.keys(this.object.controls);
        break;
      case 'owner':
        this.object = new FormGroup({
          FirstName: new FormControl(''),
          LastName: new FormControl(''),
          StreetName: new FormControl(''),
          HouseNumber: new FormControl(null),
          PostalCode: new FormControl(null),
          City: new FormControl(''),
          Country: new FormControl(''),
          PhoneNumber: new FormControl(null),
          BankInformation: new FormControl(null),
        });
        this.properties = Object.keys(this.object.controls);
        break;
      case 'flat':
        this.object = new FormGroup({
          LivingSpace: new FormControl(null),
          RentAmount: new FormControl(null),
          Date: new FormControl(''),
        });
        this.apiService.getProperties().subscribe(props => {
          this.$properties = props;
        });
        this.properties = Object.keys(this.object.controls);
        break;
    }
  }

  submit(): void {
    if (this.data == 'tenant') {
      this.object.addControl('FlatNr', new FormControl(this.foreignKey));
      this.apiService.createTenant(this.object.value).subscribe();
    } else if (this.data == 'owner') {
      this.apiService.createOwner(this.object.value).subscribe();
    } else if (this.data == 'property') {
      this.object.addControl('OwnerNr', new FormControl(this.foreignKey));
      this.apiService.createProperty(this.object.value).subscribe();
    } else if (this.data == 'flat') {
      this.object.addControl('PropertyNr', this.foreignKey);
      this.apiService.createFlat(this.object.value).subscribe();
    }
    this.matDialog.close(this.object.value);
  }

}
