import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { DataTableComponent } from './data-table/data-table.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { CreateEntryComponent } from './components/create-entry/create-entry.component';
import { AuswahlComponent } from './components/auswahl/auswahl.component';
import {HttpClientModule} from '@angular/common/http';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { ChoosetenantComponent } from './components/auswahl/choosetenant/choosetenant.component';
import { ChoosePropertyComponent } from './components/auswahl/chooseproperty/choose-property.component'
import {DatePipe} from '@angular/common';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DataTableComponent,
    CreateEntryComponent,
    AuswahlComponent,
    ChoosetenantComponent,
    ChoosePropertyComponent,
  ],
  entryComponents:[CreateEntryComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    HttpClientModule,
    MatDialogModule,
    MatMenuModule,
    MatIconModule,
    BrowserModule,
    ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
