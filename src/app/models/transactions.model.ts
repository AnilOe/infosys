export interface Transaction {
  TransactionNr?: number;
  BookingDay: string;
  BillingDay: string;
  BookingText: string;
  PurposeText: string;
  PayerReceiver: string;
  Amount: number;
}
