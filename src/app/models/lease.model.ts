export interface Lease {
  leaseId: string;
  flatId: string;
  date: Date;
  leaseAmount: number;
  flatAmount: number;
  payerId: string;
}
