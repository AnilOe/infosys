export interface Flat {
  FlatNr: number;
  propertyId: string;
  livingSpace: number;
}
