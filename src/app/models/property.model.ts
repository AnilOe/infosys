export interface Property {
  PropertyNr: string;
  OwnerNr: string;
  Streetname: string;
  HouseNumber: number;
  Postalcode: number;
  City: string;
  Country: string;
}
