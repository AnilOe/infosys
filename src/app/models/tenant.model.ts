export interface Tenant {
  TenantNr: number;
  FlatNr: number;
  FirstName: string;
  LastName: string;
  PhoneNumber: number;
  AmountOfPeople: number;
}
