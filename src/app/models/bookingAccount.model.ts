export interface BookingAccount {
  bookingId: string;
  propertyId: string;
  tenantId: string;
  bookingDay: Date;
  billingDay: Date;
  totalAmount: number;
  allocationType: string;
}
