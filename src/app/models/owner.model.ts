export interface Owner {
  OwnerNr: number;
  FirstName: string;
  lastName: string;
  streetName: string;
  houseNumber: number;
  postalCode: number;
  city: string;
  country: string;
  phoneNumber: number;
  bankInformation: number;
}
