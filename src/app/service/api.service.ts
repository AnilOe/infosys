import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {catchError, EMPTY, Observable} from 'rxjs';
import {Owner} from '../models/owner.model';
import {Tenant} from '../models/tenant.model';
import {Property} from '../models/property.model';
import {BookingAccount} from '../models/bookingAccount.model';
import {Flat} from '../models/flat.model';
import {Transaction} from '../models/transactions.model';


@Injectable({
  providedIn: 'root'
})

export class ApiService {
  constructor(private readonly http: HttpClient) {
  }

  url = environment.API_URL;

  getOwners(): Observable<Owner[]> {
    return this.http.get<Owner[]>(this.url + 'owner');
  }

  getTenants(): Observable<Tenant[]> {
    return this.http.get<Tenant[]>(this.url + 'tenant');
  }

  getProperties(): Observable<Property[]> {
    return this.http.get<Property[]>(this.url + 'property');
  }

  getAccount(id: string, year: string): Observable<BookingAccount> {
    return this.http.get<BookingAccount>(this.url + 'booking' + `/${id}/${year}`);
  }

  getFlats(): Observable<Flat[]> {
    return this.http.get<Flat[]>(this.url + 'flat');
  }

  getProperty(id: string): Observable<any> {
    return this.http.get<any>(this.url + 'property/' + id);
  }

  createOwner(owner: Owner): Observable<Owner> {
    return this.http.post<Owner>(this.url + 'owner', owner);
  }

  importCsv(transaction: Transaction[]): Observable<Transaction[]> {
    // const header = {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'};
    return this.http.post<any>(this.url + 'transaction', transaction)
      .pipe(
        catchError(() => EMPTY)
      );
  }

  createTenant(tenant: Tenant): Observable<Tenant> {
    return this.http.post<Tenant>(this.url + 'tenant', tenant);
  }

  createProperty(property: Property): Observable<Property> {
    return this.http.post<Property>(this.url + 'property', property);
  }

  createFlat(flat: Flat): Observable<Flat> {
    return this.http.post<Flat>(this.url + 'flat', flat);
  }
}
