import {Component, OnInit, ViewChild} from '@angular/core';
import {Tenant} from './models/tenant.model';
import {Property} from './models/property.model';
import {Owner} from './models/owner.model';
import {Flat} from './models/flat.model';
import jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import {ApiService} from './service/api.service';
import {BookingAccount} from './models/bookingAccount.model';
import {Transaction} from './models/transactions.model';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('csvReader') csvReader: any;
  public records: any[] = [];

  constructor(private apiService: ApiService, private datePipe: DatePipe) {}

  title = 'InfoSysProjekt';
  data: Tenant[] | Property[] | Owner[] | Flat[] | BookingAccount | undefined;
  tableCols: string[] = [];

  splitCsv(evt: any) {
    let files = evt.target.files;
    let file = files[0];
    let reader = new FileReader();
    reader.readAsText(file);
    reader.onload = () => {
      let csvData = reader.result;
      let csvRecordsArray = (<string> csvData).split(/\r\n|\n/);
      let transactions: Transaction[] = [];
      for (let val of csvRecordsArray) {
        if (val != csvRecordsArray[0]) {
          let curruntRecord = val.split(';');
          let newTransaction = {
            BookingDay: this.formatDate(curruntRecord[0]),
            BillingDay: this.formatDate(curruntRecord[1]),
            BookingText: curruntRecord[2],
            PurposeText: curruntRecord[3],
            PayerReceiver: curruntRecord[4],
            Amount: Number.parseInt(curruntRecord[5]),
          };
          transactions.push(newTransaction);
        }
      }
      transactions.pop();
      this.apiService.importCsv(transactions).subscribe();
    };

  }

  formatDate(str: string): string {
    let formDate;
    if (str) {
      const parts = str.split('.');
      // @ts-ignore
      const newDate = new Date(+parts[2], parts[1] - 1, +parts[0]);
      formDate = this.datePipe.transform(newDate, 'yyyy-MM-dd');
    }
    return (<string> formDate);
  }

  ngOnInit() {
    this.apiService.getTenants().subscribe(tenants => {
      this.data = tenants;
      this.createTableCols(tenants);
    });
  }

  generatePDF() {
    let data = document.getElementById('contentToConvert')!;
    html2canvas(data).then(canvas => {
      let imgWidth = 208;
      let imgHeight = canvas.height * imgWidth / canvas.width;
      const contentDataURL = canvas.toDataURL('image/png');
      let pdf = new jspdf('p', 'mm', 'a4');
      pdf.addImage(contentDataURL, 'PNG', 1, 1, imgWidth, imgHeight);
      pdf.save('newPDF.pdf');
    });
  }

  getData(val: any) {
    switch (val) {
      case 'tenant':
        this.apiService.getTenants().subscribe(tenants => {
          this.data = tenants;
          this.createTableCols(this.data);
        });
        break;
      case 'property':
        this.apiService.getProperties().subscribe(properties => {
          this.data = properties;
          this.createTableCols(this.data);
        });
        break;
      case 'owner':
        this.apiService.getOwners().subscribe(owner => {
          this.data = owner;
          this.createTableCols(this.data);
        });
        break;
      case 'flat':
        this.apiService.getFlats().subscribe(flats => {
          this.data = flats;
          this.createTableCols(this.data);
        });
        break;
      default:
        if (typeof val == 'string') {
          this.apiService.getProperty(val).subscribe(property => {
            this.data = property;
            this.createTableCols(property);
          });
          break;
        } else if (val.tenant && val.year) {
          this.apiService.getAccount(val.tenant, val.year).subscribe(account => {
            this.data = account;
            this.createTableCols(this.data);
          });
          break;
        } else {
          console.log(typeof this.data);
          break;
        }
    }
    return this.data;
  }

  createTableCols(data: any) {
    this.tableCols = [];
    for (let i = 0; i < Object.keys(data[0]).length; i++) {
      this.tableCols.push(Object.keys(data[0])[i]);
    }
  }
}
